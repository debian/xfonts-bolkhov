xfonts-bolkhov (1.1.20001007-10) unstable; urgency=medium

  * QA upload.
  * debian/compat: Drop obsolete file.
  * debian/control.m4: Drop obsolete file as well. We do not want
    hacky debian/control generation as well.
  * debian/control: Add Vcs-* fields on Debian Salsa GitLab.

 -- Boyuan Yang <byang@debian.org>  Tue, 19 Mar 2024 21:35:17 -0400

xfonts-bolkhov (1.1.20001007-9) unstable; urgency=medium

  * QA upload.
  * d/changelog: Remove editor cruft.
  * Convert to source format 3.0 (quilt).
  * d/rules: Add missing targets. (Closes: #999281)

 -- Bastian Germann <bage@debian.org>  Mon, 15 May 2023 23:25:01 +0200

xfonts-bolkhov (1.1.20001007-8.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Bump dependency on trscripts to fix generated fonts when using mawk, cf.
    #979599.

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 05 Jun 2021 23:47:31 +0200

xfonts-bolkhov (1.1.20001007-8.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 16:37:47 +0100

xfonts-bolkhov (1.1.20001007-8) unstable; urgency=medium

  * QA upload.
  [ Chris Lamb ]
  * debian/rules: Add -n option to gzip call. Closes: #778228.

 -- Santiago Vila <sanvila@debian.org>  Thu, 27 Aug 2015 20:00:52 +0200

xfonts-bolkhov (1.1.20001007-7) unstable; urgency=low

  * QA upload.
  * Change maintainer to QA Group.
  * debian/ cleanup:
    - debhelper compat level 9
    - change Section to fonts
    - reduce unnecessary documentation

 -- Frank Lichtenheld <djpig@debian.org>  Mon, 24 Dec 2012 17:57:41 +0100

xfonts-bolkhov (1.1.20001007-6) unstable; urgency=low

  * Depend on xfonts-utils instead of xutils.  Thanks to Michael Prokop,
    closes: #363398.
  * Remove the hack to work around #362820.  Build-depend on debhelper (>=
    5.0.32)

 -- Anton Zinoviev <zinoviev@debian.org>  Wed, 19 Apr 2006 10:57:15 +0300

xfonts-bolkhov (1.1.20001007-5) unstable; urgency=low

  * Updated for the new modular X.  Closes: #362343, #362347, #362349, #362350,
    #362351, #362352, #362353, #362354, #362355, #362360.
  * Updated standards version (3.6.6.2).

 -- Anton Zinoviev <zinoviev@debian.org>  Sat, 15 Apr 2006 23:33:25 +0300

xfonts-bolkhov (1.1.20001007-4) unstable; urgency=low

  * The upstream fonts lack pseudographic symbols.  Previously trbdf
    approximated them by ascii symbols.  Now trbdf version 1.11
    approximates them by termsymbols with codes <=0x1f when appropriate.
    Thanks to Victor B. Wagner (closes: #191215).
  * Updated `Standards-Version': 3.5.10.
  * The files in /etc/X11/fonts are not executables any more. ;-)
  * copyright points to the right place of GPL.

 -- Anton Zinoviev <zinoviev@debian.org>  Sat,  7 Jun 2003 22:40:36 +0200

xfonts-bolkhov (1.1.20001007-3) unstable; urgency=low

  * $(call foreachenc,...) and $(call foreachcat,...) eliminated from
    debian/rules.  This is workaround #175714.  Thanks to Daniel Schepler
    (closes: #169854). 
  * Used `Build-Depends' instead of `Build-Depends-Indep'.
  * Used `unicode_small' in trbdf instead of `unicode'.

 -- Anton Zinoviev <zinoviev@debian.org>  Thu, 14 Jan 2003 11:10:54 +0200

xfonts-bolkhov (1.1.20001007-2) unstable; urgency=low

  * New binary packages xfonts-bolkhov-{75dpi,misc} with Unicode fonts.

 -- Anton Zinoviev <zinoviev@debian.org>  Sat, 23 Jun 2001 22:49:44 +0300

xfonts-bolkhov (1.1.20001007-1) unstable; urgency=low

  * New upstream version.
  * Double exclamation signs in *.alias files.

 -- Anton Zinoviev <zinoviev@debian.org>  Tue, 17 Apr 2001 13:20:31 +0300

xfonts-bolkhov (1.1.20000930-6) unstable; urgency=low

  * A complete remake and Debconf in use.
  * The packages xfonts-bolkhov-${ENC} are splitted in two packages:
    xfonts-bolkhov-${ENC}-misc and xfonts-bolkhov-${ENC}-75dpi.  This
    is a requirement of the latest Debian policy.
  * Standards-Version: 3.5.2.0
  * The foundry field of all fonts is set to -rfx-.  Now there are no font
    name conflicts with the package xfonts-base.

 -- Anton Zinoviev <zinoviev@debian.org>  Tue, 27 Mar 2001 14:22:03 +0200

xfonts-bolkhov (1.1.20000930-5) unstable; urgency=medium

  * Fixed a GRAVE bug in `postrm' -- the previous version was
    uninstallable.

 -- Anton Zinoviev <zinoviev@debian.org>  Sat, 30 Dec 2000 21:11:03 +0200

xfonts-bolkhov (1.1.20000930-4) unstable; urgency=low

  * KOI8-R fonts are moved from KOI8-R.{misc,75dpi} to
    koi8-r.{misc,75dpi}.

 -- Anton Zinoviev <zinoviev@debian.org>  Sun, 19 Nov 2000 15:52:24 +0200

xfonts-bolkhov (1.1.20000930-3) unstable; urgency=low

  * KOI8-R fonts are installed in KOI8-R.misc, KOI8-R.75dpi and
    KOI8-R.100dpi directories.
  * Changes in the fonts.alias files.

 -- Anton Zinoviev <zinoviev@debian.org>  Thu, 16 Nov 2000 12:06:50 +0200

xfonts-bolkhov (1.1.20000930-2) unstable; urgency=low

  * The fonts are installed in the standard directories
    /usr/X11R6/lib/X11/fonts/{misc,75dpi,100dpi}.
  * New fonts.alias is used.  (There was colision in the names with other
    fonts.aliases.
  * In the foundry of all fields is used RFX (Cyrillic Raster Fonts for
    X11).  In the upstream in most of the fonts are used registered
    trademarks such as Adobe.

 -- Anton Zinoviev <anton@lml.bas.bg>  Wed,  4 Oct 2000 20:22:03 +0300

xfonts-bolkhov (1.1.20000930-1) unstable; urgency=low

  * New upstream version.

 -- Anton Zinoviev <anton@lml.bas.bg>  Wed,  4 Oct 2000 08:31:44 +0300

xfonts-bolkhov (1.1-2) unstable; urgency=low

  * Package with fonts for ECMA-Cyrillic is generated.

 -- Anton Zinoviev <anton@lml.bas.bg>  Sat, 10 Jun 2000 14:30:09 +0300

xfonts-bolkhov (1.1-1) unstable; urgency=low

  * Initial release.

 -- Anton Zinoviev <anton@lml.bas.bg>  Wed,  3 May 2000 21:27:19 +0300
