#!/usr/bin/make -f

# Copyright (C) 2000,2001,2003 Anton Zinoviev <zinoviev@debian.org>

# This software may be used, modified, copied, distributed, and sold,
# in both source and binary form provided that the copyright
# and these terms are retained. Under no circumstances is the author
# responsible for the proper functioning of this software, nor does
# the author assume any responsibility for damages incurred with its use.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

SHELL = /bin/bash

PACKAGE_NAME = xfonts-bolkhov
FOUNDRIES = rfx
rfx_FONTS = misc/*.bdf 75dpi/*.bdf
CATEGORIES = 75dpi misc
# There must be no fonts in one category with same names
75dpi_FONTS = 75dpi/*.bdf
misc_FONTS = misc/*.bdf
SOURCE_ENCODING = unicode
ENCODINGS = unicode cp1251 isocyr koi8r koi8u
SHORT_ENCODINGS = c w i r u
STANDARD_ENCODINGS = unicode_small cp1251 iso-8859-5 koi8-r koi8-u
XNAME_ENCODINGS = iso10646-1 microsoft-cp1251 iso8859-5 koi8-r koi8-u
DOCUMENTATION_FILES = doc/cyr-rfx_logo.gif doc/mk8859alias.pl doc/README.en.html doc/README.ru.html doc/test_text.html
UPSTREAM_CHANGELOG = doc/ChangeLog

# This target must build the BDF fonts.
build_source : ; touch build_source
# This target must clean the source package
.PHONY : clean_source
clean_source : ; -rm build_source

##############################################################
# Nothing needs to be changed below that line
##############################################################

pkg_and_short = $(join $(ENCODINGS), $(addprefix /, $(SHORT_ENCODINGS)))
pkg_and_std = $(join $(ENCODINGS), $(addprefix /, $(STANDARD_ENCODINGS)))
pkg_and_xname = $(join $(ENCODINGS), $(addprefix /, $(XNAME_ENCODINGS)))
pkg2short = $(notdir $(filter $(1)/%, $(pkg_and_short)))
pkg2std = $(notdir $(filter $(1)/%, $(pkg_and_std)))
pkg2xname = $(notdir $(filter $(1)/%, $(pkg_and_xname)))
# The variable encoding is target-specific
encoding_short = $(call pkg2short,$(encoding))
encoding_std = $(call pkg2std,$(encoding))
encoding_xname = $(call pkg2xname,$(encoding))

wrkfontdirs = $(foreach cat, $(CATEGORIES), $(addsuffix -$(cat),$(ENCODINGS)))
$(wrkfontdirs) :
	mkdir $@

convertors = $(foreach enc, $(ENCODINGS), \
			$(addsuffix -$(enc)-conv,$(FOUNDRIES)))
$(convertors) : foundry_enc = $(subst -,/,$(@:-conv=))
$(convertors) : foundry = $(subst /,,$(dir $(foundry_enc)))
$(convertors) : encoding = $(notdir $(foundry_enc))
$(convertors) :
	trbdf --foundry=$(foundry) -f $(SOURCE_ENCODING) \
		-t $(encoding_std) -s >$@

category_names = $(basename $(notdir $(wildcard $($(1)_FONTS))))
base_cat2origin = $(filter %/$(1).bdf, $(wildcard $($(2)_FONTS)))

# (category)/(base font name)
cat_and_font = $(foreach cat, $(CATEGORIES), \
			$(addprefix $(cat)/, $(call category_names,$(cat))))
# (encoding)-(category)/(base font name)_(short encoding name)
my_fonts = $(foreach enc, $(ENCODINGS), $(addprefix $(enc)-, \
		$(addsuffix _$(call pkg2short,$(enc)), $(cat_and_font))))
my_bdf_fonts = $(addsuffix .bdf, $(my_fonts))
my_pcf_fonts = $(addsuffix .pcf, $(my_fonts))
my_pcfgz_fonts = $(addsuffix .pcf.gz, $(my_fonts))

$(my_bdf_fonts) : enc_cat = $(subst /,,$(dir $@))
$(my_bdf_fonts) : encoding = $(subst /,,$(dir $(subst -,/,$(enc_cat))))
$(my_bdf_fonts) : category = $(notdir $(subst -,/,$(enc_cat)))
$(my_bdf_fonts) : basename = $(patsubst %_$(encoding_short).bdf,%,$(notdir $@))
$(my_bdf_fonts) : origin = $(call base_cat2origin,$(basename),$(category))
$(my_bdf_fonts) : font_foundry = $(addsuffix /$(1), $(wildcard $($(1)_FONTS)))
$(my_bdf_fonts) : all_font_foundry = $(foreach found, $(FOUNDRIES), \
					$(call font_foundry,$(found)))
$(my_bdf_fonts) : my_font_foundry = $(filter $(origin)/%, $(all_font_foundry))
$(my_bdf_fonts) : foundry = $(patsubst $(origin)/%,%,$(my_font_foundry))
# Unfortunately % is not expanded when the functions are computed,
# we can not use dinamic prerequisites and here we depend on all:
$(my_bdf_fonts) : build_source $(convertors)
	awk -f $(foundry)-$(encoding)-conv <$(origin) >$@

$(my_pcf_fonts) : %.pcf : %.bdf
	bdftopcf $(@:.pcf=.bdf) >$@

$(my_pcfgz_fonts) : %.pcf.gz : %.pcf
	gzip -9n <$(@:.pcf.gz=.pcf) >$@

all_fonts : $(my_pcfgz_fonts)

cat_alias = $(addsuffix .alias, $(CATEGORIES))
source_aliases = $(wildcard $(addprefix debian/, $(cat_alias)))
package_aliases = $(foreach enc, $(ENCODINGS), \
		$(addprefix debian/$(enc)-, $(notdir $(source_aliases))))

$(package_aliases) : enc_cat = $(basename $(notdir $@))
$(package_aliases) : encoding = $(subst /,,$(dir $(subst -,/,$(enc_cat))))
$(package_aliases) : category = $(notdir $(subst -,/,$(enc_cat)))
# Again, here we cannot generate dinamically prerequisites:
$(package_aliases) : $(source_aliases)
	sed -e "s/-WEND-ENCODING/-$(encoding_xname)/g" -e \
		"s/PREFIX/$(encoding_short)/g" debian/$(category).alias >$@

all_aliases : $(package_aliases)

.PHONY : build-indep
build-indep : build_source $(wrkfontdirs)
	$(MAKE) -f debian/rules all_fonts
	$(MAKE) -f debian/rules all_aliases

.PHONY : my_clean
my_clean : clean_source
	-rm -rf $(wrkfontdirs)
	-rm -f $(convertors)
	-rm -f $(package_aliases)
	-rm -f my_clean

packages = $(addprefix $(PACKAGE_NAME)-, $(wrkfontdirs))

$(packages) : enc_cat = $(patsubst $(PACKAGE_NAME)-%, %, $@)
$(packages) : encoding = $(subst /,,$(dir $(subst -,/,$(enc_cat))))
$(packages) : category = $(notdir $(subst -,/,$(enc_cat)))
$(packages) : alias_file = debian/$(encoding)-$(category).alias
$(packages) : pack_enc = $(addprefix $(PACKAGE_NAME)-, $(encoding))
$(packages) : deb_name = $(addprefix $(pack_enc:-unicode=)-, $(category))
.PHONY : $(packages)
$(packages) :
	[ ! -f $(alias_file) ] || install -m644 -D $(alias_file) \
		debian/$(deb_name)/etc/X11/fonts/X11R7/$(category)/$(deb_name).alias
	install -d debian/$(deb_name)/usr/share/fonts/X11/$(category)/
	cp $(encoding)-$(category)/*.pcf.gz \
		debian/$(deb_name)/usr/share/fonts/X11/$(category)/

.PHONY : my_install
my_install : $(packages)

build : build-indep
	dh_testdir
	touch build

.PHONY : clean
clean:
	dh_testdir
	rm -f build
	$(MAKE) -f debian/rules my_clean
	dh_clean

.PHONY : install
install: build
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs
	$(MAKE) -f debian/rules my_install

# Build architecture-independent files here.
.PHONY : binary-indep
binary-indep: build install
	dh_testdir
	dh_testroot
	dh_installxfonts
#	dh_installdebconf
	dh_installdocs -A $(DOCUMENTATION_FILES)
#	dh_installexamples
#	dh_installman
#	dh_installinfo
	dh_installchangelogs $(UPSTREAM_CHANGELOG)
#	dh_link
	dh_compress
	dh_fixperms
	dh_installdeb
	dh_gencontrol
	dh_md5sums
	dh_builddeb

# Build architecture-dependent files here.
.PHONY : binary-arch build-arch
binary-arch : build install
build-arch : build
# We have nothing to do by default.

.PHONY : binary
binary : binary-indep binary-arch
